# Lindo-Matabane-module-2

## M2-Assessment 1

### Instructions

- [x] Create a Gitlab account.
- [x] Create a repository on Gitlab as name-surname-module-2.
- [x] Create 3 .dart files for the exercise.
- [x] All code outputs are printed on the console.

### Assessment

All the code must be submitted through a Gitlab repository link. The code must be on three files, each with this code:

- [x] Write a basic program that stores and then prints the following data: Your name, favorite app, and city.
- [x] Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; a) Sort and print the apps by name;  b) Print the winning app of 2017 and the winning app of 2018.; c) the Print total number of apps from the array.
- [x] Create a class and a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. Create b) a function inside the class, transform the app name to all capital letters and then print the output.