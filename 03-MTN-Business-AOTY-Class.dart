class App {
  var name = "LIVE Inspect";
  var category = "Best Android App(Enterprise)";
  var year = 2014;
  capitalize() {
    return name.toUpperCase();
  }
}
void main() {
  App myapp = new App();
  print("App Name: ${myapp.name}\nApp Category: ${myapp.category}\nApp Year: ${myapp.year}");
  print(myapp.capitalize());
}